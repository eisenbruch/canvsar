//
//  ImageDetectViewController.swift
//  canvsAR
//
//  Created by Noah Eisenbruch on 4/8/18.
//  Copyright © 2018 CANVS. All rights reserved.
//

import UIKit
import SceneKit
import ARKit
import SpriteKit
import AVKit
import AVFoundation
//import GPUImage

class ImageDetectViewController: UIViewController, ARSCNViewDelegate, ARSessionDelegate, SKViewDelegate  {

    @IBOutlet weak var imageDetectView: ARSCNView!

    // Do any additional setup after loading the view.
    override func viewDidLoad() {
        super.viewDidLoad()
        imageDetectView.delegate = self
        imageDetectView.session.delegate = self
//        imageDetectView.showsStatistics = true
        imageDetectView.autoenablesDefaultLighting = true
        let ARScene = SCNScene()
        imageDetectView.scene = ARScene
        
        imageDetectView.antialiasingMode = .multisampling4X
    }
    
    // Adding viewWillAppear function
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        // Setting up our scene configuration
        let configuration = ARWorldTrackingConfiguration()
        configuration.planeDetection = [.vertical, .horizontal]
        configuration.detectionImages = ARReferenceImage.referenceImages(inGroupNamed: "AR Resources", bundle: nil)
        
        imageDetectView.session.run(configuration)
    }
    
    // Dispose of any resources that can be recreated.
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        
    }
    
    func renderer(_ renderer: SCNSceneRenderer, nodeFor anchor: ARAnchor) -> SCNNode? {
        let anchorNode = SCNNode()
        anchorNode.name = "anchor"
        imageDetectView.scene.rootNode.addChildNode(anchorNode)
        return anchorNode
    }
    
    func renderer(_ renderer: SCNSceneRenderer, didAdd node: SCNNode, for anchor: ARAnchor) {
        // If we find an image...
        guard let imageAnchor = anchor as? ARImageAnchor else {return}
        if let imageName = imageAnchor.referenceImage.name {
            // If we see the mural
            if imageName == "painting" || imageName == "wigz" {
                DispatchQueue.main.async {
                    // Get Video URL and create AV Player
                    let filePath = Bundle.main.path(forResource: "wigz", ofType: "mp4")
                    let videoURL = NSURL(fileURLWithPath: filePath!)
                    let player = AVPlayer(url: videoURL as URL)

                    // Create SceneKit videoNode to hold the spritekit scene.
                    let videoNode = SCNNode()
                    
                    // Set geometry of the SceneKit node to be a plane, and rotate it to be flat with the image
                    videoNode.geometry = SCNPlane(width: imageAnchor.referenceImage.physicalSize.width,
                                                  height: imageAnchor.referenceImage.physicalSize.height)
                    videoNode.eulerAngles = SCNVector3(-Float.pi/2, 0, 0)
                    
                    //Set the video AVPlayer as the contents of the video node's material.
                    videoNode.geometry?.firstMaterial?.diffuse.contents = player
                    videoNode.geometry?.firstMaterial?.isDoubleSided = true
                    
                    // Alpha transparancy stuff
                    let chromaKeyMaterial = ChromaKeyMaterial()
                    chromaKeyMaterial.diffuse.contents = player
                    videoNode.geometry!.materials = [chromaKeyMaterial]
                    
                    //video does not start without delaying the player
                    //playing the video before just results in [SceneKit] Error: Cannot get pixel buffer (CVPixelBufferRef)
                    DispatchQueue.main.asyncAfter(deadline: .now() + 0.001) {
                        player.seek(to:CMTimeMakeWithSeconds(1, 1000))
                        player.play()
                    }
                    // Loop video
                    NotificationCenter.default.addObserver(forName: .AVPlayerItemDidPlayToEndTime, object: player.currentItem, queue: .main) { _ in
                        player.seek(to: kCMTimeZero)
                        player.play()
                    }
                    
                    // Add videoNode to ARAnchor
                    node.addChildNode(videoNode)
                    
                    // Add ARAnchor node to the root node of the scene
                    self.imageDetectView.scene.rootNode.addChildNode(node)
                                    
                    ///////// DAE STUFF
//                    var nodeModel:SCNNode!
//                    let monkeyScene = SCNScene(named: "monkey.dae")
//                    nodeModel = monkeyScene?.rootNode.childNode(withName: "Suzanne", recursively: true)
//                    nodeModel.position = node.position
//
//
//                    self.imageDetectView.scene.rootNode.addChildNode(nodeModel)

                }

            }
            
            if imageName == "joesbackyward" {
                DispatchQueue.main.async {
                    // Get Video URL and create AV Player
                    let filePath = Bundle.main.path(forResource: "joesbackyward", ofType: "mp4")
                    let videoURL = NSURL(fileURLWithPath: filePath!)
                    let player = AVPlayer(url: videoURL as URL)
                    
                    // Create SceneKit videoNode to hold the spritekit scene.
                    let videoNode = SCNNode()
                    
                    // Set geometry of the SceneKit node to be a plane, and rotate it to be flat with the image
                    videoNode.geometry = SCNPlane(width: imageAnchor.referenceImage.physicalSize.width,
                                                  height: imageAnchor.referenceImage.physicalSize.height)
                    videoNode.eulerAngles = SCNVector3(-Float.pi/2, 0, 0)
                    
                    //Set the video AVPlayer as the contents of the video node's material.
                    videoNode.geometry?.firstMaterial?.diffuse.contents = player
                    videoNode.geometry?.firstMaterial?.isDoubleSided = true
                    
                    // Alpha transparancy stuff
                    let chromaKeyMaterial = ChromaKeyMaterial()
                    chromaKeyMaterial.diffuse.contents = player
                    videoNode.geometry!.materials = [chromaKeyMaterial]
                    
                    //video does not start without delaying the player
                    //playing the video before just results in [SceneKit] Error: Cannot get pixel buffer (CVPixelBufferRef)
                    DispatchQueue.main.asyncAfter(deadline: .now() + 0.001) {
                        player.seek(to:CMTimeMakeWithSeconds(1, 1000))
                        player.play()
                    }
                    // Loop video
                    NotificationCenter.default.addObserver(forName: .AVPlayerItemDidPlayToEndTime, object: player.currentItem, queue: .main) { _ in
                        player.seek(to: kCMTimeZero)
                        player.play()
                    }
                    
                    // Add videoNode to ARAnchor
                    node.addChildNode(videoNode)
                    
                    // Add ARAnchor node to the root node of the scene
                    self.imageDetectView.scene.rootNode.addChildNode(node)
                }
                
            }
            /*// If we see the painting
            if imageName == "painting" {
                DispatchQueue.main.async {
                    let sphere = SCNNode()
                    sphere.geometry = SCNSphere(radius: 0.015)
                    sphere.geometry?.firstMaterial?.diffuse.contents = UIColor.purple.withAlphaComponent(1)
                    sphere.position = SCNVector3(x: 0, y: 0, z: 0)
                    node.addChildNode(sphere)
                    self.imageDetectView.scene.rootNode.addChildNode(node)
                }
            }*/
        }
    }
    
    func placeMuralContents(node: SCNNode){
        let sphere = SCNNode()
        sphere.geometry = SCNSphere(radius: 0.015)
        sphere.geometry?.firstMaterial?.diffuse.contents = UIColor.green.withAlphaComponent(0.6)
        sphere.position = SCNVector3(x: 0, y: 0, z: 0)
        node.addChildNode(sphere)
        self.imageDetectView.scene.rootNode.addChildNode(node)
        
    }
    /*
    func renderer(_ renderer: SCNSceneRenderer, didUpdate node: SCNNode, for anchor: ARAnchor) {
        guard let planeAnchor = anchor as? ARPlaneAnchor else {return}
        let planeGeometry = planeAnchor.geometry
        guard let device = MTLCreateSystemDefaultDevice() else {return}
        let plane = ARSCNPlaneGeometry(device: device)
        plane?.update(from: planeGeometry)
        node.geometry = plane
        node.geometry?.firstMaterial?.diffuse.contents = UIColor.black
        node.geometry?.firstMaterial?.transparency = 1
        node.geometry?.firstMaterial?.fillMode = SCNFillMode.lines
    }
    */
    @IBAction func tapGesture(_ sender: UIGestureRecognizer) {
        
    }
    
    @IBAction func panGesture(_ sender: UIPanGestureRecognizer) {
        
    }
    
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
