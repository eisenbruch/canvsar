////
////  ViewController.swift
////  canvsAR
////
////  Created by Noah Eisenbruch on 4/8/18.
////  Copyright © 2018 CANVS. All rights reserved.
////
//
//import UIKit
//import SceneKit
//import ARKit
//
//class ViewController: UIViewController, ARSCNViewDelegate, ARSessionDelegate {
//
//    // Link ARSCNView to our swift viewcontroller file
//    @IBOutlet weak var pianoView: ARSCNView!
//    
//    override func viewDidLoad() {
//        super.viewDidLoad()
//        // Do any additional setup after loading the view, typically from a nib.
//        
//        // Setting up the view's delegate
//        pianoView.delegate = self
//        
//        // Show statistics about FPS, etc.
//        pianoView.showsStatistics = true
//        
//        // Enable default lighing
//        pianoView.autoenablesDefaultLighting = true
//        
//        // Create SCNscene
//        let pianoScene = SCNScene()
//        
//        // Linking the scene to the view
//        pianoView.scene = pianoScene
//        // pianoView.debugOptions = [ARSCNDebugOptions.showFeaturePoints, ARSCNDebugOptions.showWorldOrigin]
//    }
//    
//    // Adding viewWillAppear function
//    override func viewWillAppear(_ animated: Bool) {
//        super.viewWillAppear(animated)
//        // Setting up our scene configuration
//        let configuration = ARWorldTrackingConfiguration()
//        configuration.planeDetection = [.vertical, .horizontal]
//        pianoView.session.run(configuration)
//    }
//
//    override func didReceiveMemoryWarning() {
//        super.didReceiveMemoryWarning()
//        // Dispose of any resources that can be recreated.
//    }
//    
//    // Create node that we are going to position, rotate and add geometry to.
//    // Visually represents our plane.
//    let planeNode = SCNNode()
//    
//    // Function is calledwhenever a new ARAnchor is rendered in the scene.
//    // In this case whenever a horizontal plane is detected.
//    func renderer(_ renderer: SCNSceneRenderer, nodeFor anchor: ARAnchor) -> SCNNode? {
//        // Create SCNNode that we are going to return
//        let aRAncorNode = SCNNode()
//        aRAncorNode.name = "anchor"
//        pianoView.scene.rootNode.addChildNode(aRAncorNode)
//
//        // Convert the ARAncor to ARPlaneAnchor to get access to ARPlaneAnchor's values (extent, center)
//        let anchor = anchor as? ARPlaneAnchor
//        // Make plane geometry to match the width and height of the plane
//        planeNode.geometry = SCNPlane(width: CGFloat((anchor?.extent.x)!),
//                                      height: CGFloat((anchor?.extent.z)!))
//        // Transforming plane node
//        planeNode.position = SCNVector3((anchor?.center.x)!, 0, (anchor?.center.z)!)
//        planeNode.geometry?.firstMaterial?.diffuse.contents = UIImage(named: "hexmap") //UIColor.purple.withAlphaComponent(0.35)
//        // Rotate 90 degrees on X axis so plane is flat
//        planeNode.eulerAngles = SCNVector3(-Float.pi/2, 0, 0)
//        
//        // Add plane node as chile to AR Anchor node (mandatory ARKit convention)
//        //aRAncorNode.addChildNode(planeNode)
//        // Return aRAnchorNode (must return node from this function to add it to scene)
//        return aRAncorNode
//    }
//    
//    
//    // For updating anchors/planes/nodes
//    func renderer(_ renderer: SCNSceneRenderer, didUpdate node: SCNNode, for anchor: ARAnchor) {
//        // Convert the ARAncor to ARPlaneAnchor to get access to ARPlaneAnchor's values (extent, center)
//        guard let planeAnchor = anchor as? ARPlaneAnchor else {return}
//        let planeGeometry = planeAnchor.geometry
//        guard let device = MTLCreateSystemDefaultDevice() else {return}
//        let plane = ARSCNPlaneGeometry(device: device)
//        plane?.update(from: planeGeometry)
//        node.geometry = plane
//        node.geometry?.firstMaterial?.diffuse.contents = UIColor.purple.withAlphaComponent(0.75)
////        node.geometry?.firstMaterial?.transparency = 0.75
//        node.geometry?.firstMaterial?.fillMode = SCNFillMode.lines
//
//        
////        let planeNode = node.childNodes.first
////
////        // Make plane geometry to match the width and height of the plane
////        planeNode?.geometry = SCNPlane(width: CGFloat((anchor.extent.x)),
////                                       height: CGFloat((anchor.extent.z)!))
////        // Transforming plane node
////        planeNode?.position = SCNVector3((anchor.center.x), 0, (anchor.center.z)!)
////        planeNode?.geometry?.firstMaterial?.diffuse.contents = UIImage(named: "hexmap") //UIColor.purple.withAlphaComponent(0.35)
//    }
//    
//    @IBAction func tapScreenGesture(_ sender: UIGestureRecognizer) {
//        let touchPoint = sender.location(in: pianoView)
//        nodeInjector(touchPoint: touchPoint)
//    }
//    
//    // Called when user drags finger on screen
//    @IBAction func panGesture(_ sender: UIPanGestureRecognizer) {
//        let touchPoint = sender.location(in: pianoView)
//        nodeInjector(touchPoint: touchPoint)
//    }
//    
//    // Creating the lettersNode that will hold the geometry
//    // In this case a plane that has a tansparent png image as material
//    // Making this node available to entire view controller since we
//    // will call it in many functions
//    let lettersNode = SCNNode()
//    
//    func nodeInjector (touchPoint: CGPoint){
//        // removing lettersNode from its parent node. This prevents placing
//        // more than 1 set of letters
//        lettersNode.removeFromParentNode()
//        
//        // Calling a hitTest for our AR scene view and storing results in hitLocation
//        let hitLocation = pianoView.hitTest(touchPoint, types: .existingPlaneUsingExtent)
//        
//        // Basic error handling in case touched point isn't a detected plane
//        if hitLocation.isEmpty {
//            let alert = UIAlertController.init(title: ":(", message: "There is no detected plane at that location!", preferredStyle: .alert)
//            alert.addAction(UIAlertAction(title: "OK", style: .default, handler: nil))
//            self.present(alert, animated: true)
//            // Return from nodeInjector early to avoid errors since hitLocation is empty
//            return
//        }
//        
//        // Use current cam orientation, specficially yaw, to ensure when we place piano letters they are facing the cam.
//        let cameraYaw = pianoView.session.currentFrame?.camera.eulerAngles.y
//        // Prepare euler angles for letters plane: -90deg. about x-axis. Then set yaw = to cameraYaw so letters are facing us
//        let lettersRotation = SCNVector3(-Float.pi/2, cameraYaw!, 0)
//        // Prepare letters location using x y z coordinates of the first discovered hitLocation relative to world orig (root node)
//        let lettersLocation = SCNVector3(hitLocation[0].worldTransform.columns.3.x, hitLocation[0].worldTransform.columns.3.y, hitLocation[0].worldTransform.columns.3.z)
//        // Set geometry of plane to be exactly width and height keyboard keys. ARKit/SceneKit units are in meters.
//        let lettersGeometry = SCNPlane(width: 0.8382, height: 0.1397)
//        // Apply geometry to node
//        lettersNode.geometry = lettersGeometry
//        // Create material that will hold the image
//        let lettersMaterial = SCNMaterial()
//        lettersMaterial.diffuse.contents = UIImage(named: "piano-keys")
//        // Set transparency of material
//        lettersMaterial.transparency = 0.7
//        
//        // Add position, rotation, material to letters node for testing
//        lettersNode.position = lettersLocation
//        lettersNode.pivot = SCNMatrix4MakeTranslation(Float(0-lettersGeometry.width/2-0.0127), -0.05, 0)
//        lettersNode.eulerAngles = lettersRotation
//        lettersNode.geometry?.firstMaterial = lettersMaterial
//        pianoView.scene.rootNode.addChildNode(lettersNode)
//    }
//    
//
//    
//}
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
